let conferences = {};

chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
    // conference_data handler
    if (msg.text === 'conference_data') {
        // store the conferences data
        conferences = msg.data;
        // send a response to the background script
        sendResponse({text: `Message has been received by content-script from background`});
        // update the conferences
        updateConferences();
    }
});

function updateConferences() {
    // loop through conferences
    for (const [confId, conference] of Object.entries(conferences)) {
        // get the div of the conference
        let confDiv = document.querySelector(`div[jslog*="${conference.id}"]`);
        // if the div exists and doesn't already have a cost pill
        if (confDiv && !confDiv.querySelector('.cost-pill')) {
            // create the pill
            let pillDiv = createPill();
            pillDiv.innerHTML = conference.totalCost + '€';
            // add the pill to the conference div
            confDiv.appendChild(pillDiv);
        }
    }
}

function createPill() {
    let pillDiv = document.createElement('div');
    pillDiv.classList.add('cost-pill');
    pillDiv.style.position = 'absolute';
    pillDiv.style.top = '3px';
    pillDiv.style.right = '1px';
    pillDiv.style.padding = '2px 5px';
    pillDiv.style.fontSize = '10px';
    pillDiv.style.backgroundColor = '#f44336';
    pillDiv.style.color = '#fff';
    pillDiv.style.borderRadius = '5px';
    pillDiv.style.fontWeight = 'bold';
    pillDiv.style.zIndex = '1000';

    return pillDiv;
}

// Periodically update the conference costs
setInterval(updateConferences, 1000);
