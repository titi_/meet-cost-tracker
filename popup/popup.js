let upgrade = document.getElementById("upgrade");
let body = document.querySelector('body');
let display = document.getElementById('display');

upgrade.addEventListener('click', () => {
    body.style.height = "370px";
    body.style.width = "330px";
    upgrade.style.display = "none";
    display.style.display = "flex";
})

function changeOnUpgrade() {

}

function changeStyleBack() {
    on.style.color = "black";
    off.style.color = "white";
}

function changeStyle() {
    on.style.color = "white";
    off.style.color = "black";
}

chrome.runtime.sendMessage({text: 'get-errors'}, (response) => {
    if (response.message.text === 'error') {
        let error = response.message;
        // création d'une div pour afficher l'erreur
        let errorDiv = document.createElement('div');
        errorDiv.classList.add('alert');
        // ajout du texte de l'erreur
        if (error.code === 401) {
            errorDiv.innerHTML = `<strong>MeetCostTracker must have access to your Google account to work properly.</strong>`;
        }
        // récupération de la div du switch
        let upgradeDiv = document.getElementById('upgrade');
        upgradeDiv.style.marginTop = '0.75rem';
        errorDiv.style.height = "45px";
        // augmentation de la hauteur du body
        document.querySelector('body').style.height = '230px';
        // affichage de la div
        upgradeDiv.insertAdjacentElement('beforebegin', errorDiv);
    }
});

window.onload = async (event) => {
    // check if the user uses Brave
    const isBrave = navigator.brave && await navigator.brave.isBrave() || false
    // if the user uses Brave, display a warning
    if (isBrave) {
        let warning = document.createElement('div');
        warning.classList.add('alert');
        warning.innerHTML = `<strong>You are using Brave.<br> You must authorize Google login for extensions in <i>brave://settings/extensions</i> for MeetCostTracker to work properly.</strong>`;
        warning.style.height = "70px";
        let upgradeDiv = document.getElementById('upgrade');
        upgradeDiv.style.marginTop = '0.75rem';
        document.querySelector('body').style.height = '230px';
        document.querySelector('nav').style.marginBottom = '0';
        upgradeDiv.insertAdjacentElement('beforebegin', warning);

    }
};