const MAX_RESULTS = 1000;
const FETCH_INTERVAL = 3600000; // 1 hour
const CONFERENCES_TO_SHOW = 20 // number of conferences to show on the calendar

let conferencesData = null;
let lastFetch = null;
let isActive = true;
let fetchErrors = {}; 

chrome.runtime.onMessage.addListener(handleMessage);

function handleMessage(msg, sender, sendResponse) {
    // fetch potential errors
    if (msg.text === 'get-errors') {
        sendResponse({message: fetchErrors});
    }
}

// Listen for tab updates
chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    if (changeInfo.status === 'complete' && tab.url.startsWith('https://calendar.google.com/')) {
        // Refresh the content script when the calendar page is updated
        sendMessageToContentScript(tab.id);
    }
});

function sendMessageToContentScript(tabId) {
    if (conferencesData === null || (lastFetch !== null && Date.now() - lastFetch > FETCH_INTERVAL)) {
        updateConferencesData(tabId).then(() => {
            chrome.tabs.sendMessage(tabId, { text: 'conference_data', data: conferencesData, active: isActive });
        });
    } else {
        chrome.tabs.sendMessage(tabId, { text: 'conference_data', data: conferencesData, active: isActive });
    }
}

async function updateConferencesData(tabId) {
    return new Promise((resolve) => {
        // get the auth token
        chrome.identity.getAuthToken({ interactive: true }, (token) => {
            // if there is a token
            if (token) {
                // Remove the token from the cache
                chrome.identity.removeCachedAuthToken({ token: token }, function() {
                    // Get a new token
                    chrome.identity.getAuthToken({ interactive: true }, function(token) {
                        // Use the new token
                        let fetchOptions = {
                            method: 'GET',
                            headers: {
                                Authorization: `Bearer ${token}`,
                            },
                        };
                        let date = new Date();
                        // calculating the min and max dates to fetch from
                        let timeMin = (new Date(date.getFullYear(), date.getMonth() - 1, 1)).toISOString();
                        let timeMax = (new Date(date.getFullYear(), date.getMonth() + 1, 31)).toISOString();
                        // query params
                        let fetchParams = `maxResults=${MAX_RESULTS}&timeMin=${timeMin}&timeMax=${timeMax}&orderBy=StartTime&singleEvents=true`
                        // object to store the conferences
                        let conferences = {};

                        fetch(
                            `https://www.googleapis.com/calendar/v3/calendars/primary/events?${fetchParams}`,
                            fetchOptions
                        )
                            .then((response) => response.json())
                            .then(function (data) {
                                if (data.error) {
                                    // store the error to send it to the popup
                                    fetchErrors = {text: 'error', code: data.error.code};
                                } else {
                                    let events = data.items;
                                    // looping through events
                                    events.forEach(function (event) {
                                        // if event has attendees
                                        if (event.attendees && event.attendees.length > 0) {
                                            let eventTotal = 0;
                                            // adding the hourly rate of each attendee
                                            event.attendees.forEach(function (attendee) {
                                                attendee['hourCostRate'] = Math.floor(Math.random() * (40 - 20) + 20);
                                                eventTotal += attendee.hourCostRate;
                                            });
                                            // fetch the dates as Date objects
                                            const startDate = new Date(event.start.dateTime);
                                            const endDate = new Date(event.end.dateTime);
                                            // calculation of the event duration in hours
                                            const duration = (endDate - startDate) / 1000 / 60 / 60;
                                            // adding the duration and cost to the event
                                            event['duration'] = duration;
                                            event['totalCost'] = eventTotal * duration;
                                            conferences[event.id] = {'duration': event.duration, 'totalCost': event.totalCost};
                                        }
                                    });
                                    // loop through conferences to get only top 10 conferences
                                    const asArray = Object.entries(conferences).map(([key, value]) => ({id: key, ...value}));
                                    // sort the array by totalCost in descending order
                                    const sortedArray = asArray.sort((a, b) => b.totalCost - a.totalCost);
                                    // get the specified number of items of the sorted array and store conferences data
                                    conferencesData = sortedArray.slice(0, CONFERENCES_TO_SHOW);
                                    // store the last fetch date
                                    lastFetch = Date.now();

                                    resolve();
                                }
                            });
                    });
                });
            }
        });
    }).then(() => {
        // sending fetched data to the calendar tab
        chrome.tabs.sendMessage(tabId, { text: 'conference_data', data: conferencesData });
    });
}
